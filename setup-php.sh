#!/usr/bin/env bash
yum install -y php
yum install -y php-dom php-gd
yum install -y php-imagick
yum install -y php-pgsql
yum install -y php-zip
yum install -y php-mbstring
yum install -y php-memcache
yum install -y zip unzip
yum install -y poppler-utils